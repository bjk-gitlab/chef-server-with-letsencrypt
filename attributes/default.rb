default['chef-server-with-letsencrypt']['lego_email'] = ''
default['chef-server-with-letsencrypt']['lego_params'] = '--http :80'
default['chef-server-with-letsencrypt']['lego_env'] = {}
default['chef-server-with-letsencrypt']['lego_version'] = '0.4.1'
default['chef-server-with-letsencrypt']['renew_with_cron'] = true
default['chef-server-with-letsencrypt']['install_chef_server'] = true
