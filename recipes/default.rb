attr = node['chef-server-with-letsencrypt']

execute 'download and install lego' do
  cwd '/tmp'
  bin = '/usr/local/bin/lego'
  command <<-EOS
  curl -sL https://github.com/xenolf/lego/releases/download/v#{attr['lego_version']}/lego_linux_amd64.tar.xz | tar -xJO lego_linux_amd64 > #{bin}
  chmod +x #{bin}
  EOS
  creates bin
end

execute 'generate certs with lego' do
  environment attr['lego_env']
  sensitive !attr['lego_env'].empty?
  command <<-EOS
  lego --email="#{attr['lego_email']}" --domains="#{node['chef-server']['api_fqdn']}" #{attr['lego_params']} -a --path /etc/lego/ run
  EOS
  creates "/etc/lego/certificates/#{node['chef-server']['api_fqdn']}.crt"
end

# Can't use only_if - Chef run will still fail if `crontab` doesn't exist
if attr['renew_with_cron']
  cron 'lego-ssl-chef' do
    time :monthly
    command <<-EOS
    lego --email="#{attr['lego_email']}" --domains="#{node['chef-server']['api_fqdn']}" #{attr['lego_params']} -a --path /etc/lego/ renew
    EOS
  end
end

node.override['chef-server']['configuration'] = <<-EOS
bookshelf['vip'] = '#{node['chef-server']['api_fqdn']}'
nginx['server_name'] = '#{node['chef-server']['api_fqdn']}'
nginx['non_ssl_port'] = false
nginx['ssl_certificate'] = '/etc/lego/certificates/#{node['chef-server']['api_fqdn']}.crt'
nginx['ssl_certificate_key'] = '/etc/lego/certificates/#{node['chef-server']['api_fqdn']}.key'
EOS

include_recipe 'chef-server' if attr['install_chef_server']
