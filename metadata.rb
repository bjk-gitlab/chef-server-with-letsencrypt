name 'chef-server-with-letsencrypt'
description 'Installs/Configures chef-server-with-letsencrypt'
long_description File.read(File.dirname(__FILE__) + '/README.md')
issues_url 'https://gitlab.com/virtkick/chef-server-with-letsencrypt'
source_url 'https://gitlab.com/virtkick/chef-server-with-letsencrypt'

maintainer 'nowaker'
maintainer_email 'nowaker@virtkick.com'

license 'Apache v2.0'

version '1.0.0'
chef_version '>= 12.7' if respond_to?(:chef_version)

depends 'chef-server'
depends 'cron'
